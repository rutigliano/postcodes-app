/**
 * App Init 
 *
 *
 * @author  Domenico Rutigliano wdetcetera@gmail.com
 * 
 */
import React, { Component } from 'react';
import './App.css';
import FormContainer from './containers/FormContainer';

class App extends Component {
  render() {
    return (
      <div className="row col-md-6">
        <h3> Sample Postcode Checker </h3>
        <FormContainer />
      </div>
    );
  }
}

export default App;