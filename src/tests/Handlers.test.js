/**
 * Handlers Unit Tests
 *
 *
 * @author  Domenico Rutigliano wdetcetera@gmail.com
 * 
 */


import FormContainer from '../containers/FormContainer';


describe("Input component", () => {
    test("Returning matching lacality", () => {
        const locality = FormContainer.handlePostcodeSearch(
            {
                "locality": [
                    { "category": "Post Office Boxes", "id": 575, "location": "CHATSWOOD", "postcode": 2057, "state": "NSW" },
                    { "category": "Delivery Area", "id": 604, "latitude": -33.79626418, "location": "CHATSWOOD", "longitude": 151.181117, "postcode": 2067, "state": "NSW" },
                    { "category": "Delivery Area", "id": 605, "latitude": -33.79503737, "location": "CHATSWOOD WEST", "longitude": 151.162232, "postcode": 2067, "state": "NSW" },
                    { "category": "Post Office Boxes", "id": 416, "location": "WEST CHATSWOOD", "postcode": 1515, "state": "NSW" }]
            },

            2067);
        expect(locality).toEqual({ "category": "Delivery Area", "id": 604, "latitude": -33.79626418, "location": "CHATSWOOD", "longitude": 151.181117, "postcode": 2067, "state": "NSW" });
    });
});



