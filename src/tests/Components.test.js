/**
 * Components Unit Tests
 *
 *
 * @author  Domenico Rutigliano wdetcetera@gmail.com
 * 
 */


import React from 'react';
import Input from '../components/Input';
import Button from '../components/Button';
import Output from '../components/Output';
import Select from '../components/Select';
import { create } from "react-test-renderer";


describe("Input component", () => {
    test("Matches the snapshot", () => {
        const input = create(<Input />);
        expect(input.toJSON()).toMatchSnapshot();
    });
});

describe("Button component", () => {
    test("Matches the snapshot", () => {
        const button = create(<Button
            type={"primary"}
            title={"Submit"} />);
        expect(button.toJSON()).toMatchSnapshot();
    });
});

describe("Notification component", () => {
    test("Matches the snapshot", () => {
        const mock =  {Message:"This is a test"};
        const output = create(<Output  data={mock}/>);
        expect(output.toJSON()).toMatchSnapshot();
    });
});

describe("Select component", () => {
    test("Matches the snapshot", () => {
        const satesOptions= ['a','b','c'];
        const select = create(<Select options={satesOptions}/>);
        expect(select.toJSON()).toMatchSnapshot();
    });
});

//{"localities":{"locality":[{"category":"Post Office Boxes","id":575,"location":"CHATSWOOD","postcode":2057,"state":"NSW"},{"category":"Delivery Area","id":604,"latitude":-33.79626418,"location":"CHATSWOOD","longitude":151.181117,"postcode":2067,"state":"NSW"},{"category":"Delivery Area","id":605,"latitude":-33.79503737,"location":"CHATSWOOD WEST","longitude":151.162232,"postcode":2067,"state":"NSW"},{"category":"Post Office Boxes","id":416,"location":"WEST CHATSWOOD","postcode":1515,"state":"NSW"}]}}