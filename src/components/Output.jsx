import React from "react";

const Output = props => {
  return (
    <div
      role="alert"
      className={
        props.show === "hide" ? "invisible" : "alert alert-" + props.status
      }
    >
      <p>{props.data.Message}</p>
    </div>
  );
};

export default Output;
