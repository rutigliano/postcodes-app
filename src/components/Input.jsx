import React from "react";

const Input = props => {
  //console.log(props);
  return (
    <div className="form-group">
      <label html={props.name} className="form-label">
        {props.title}
      </label>
      <input
        className="form-control"
        required={props.required}
        id={props.name}
        name={props.name}
        inputtype={props.inputType}
        value={props.value}
        placeholder={props.placeholder}
        {...props}
      />
    </div>
  );
};

export default Input;
