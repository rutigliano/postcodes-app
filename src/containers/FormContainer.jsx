/**
 * App Container
 *
 *
 * @author  Domenico Rutigliano wdetcetera@gmail.com
 *
 */

import React, { Component } from "react";

/* Import Components */
import Input from "../components/Input";
import Select from "../components/Select";
import Button from "../components/Button";
import Output from "../components/Output";

export class FormContainer extends Component {
  constructor(props) {
    super(props);
    this.form = React.createRef();
    this.handleValidation = this.handleValidation.bind(this);
    this.state = {
      submission: {
        suburb: "",
        postcode: "",
        cState: "",
        successResult: { Message: "" }
      },

      satesOptions: ["NSW", "VIC", "QLD", "WA", "NT", "ACT"],
      show: "hide"
    };
    this.handleSelect = this.handleSelect.bind(this);
    this.handleSuburb = this.handleSuburb.bind(this);
    this.handleState = this.handleState.bind(this);
    this.handlePostcode = this.handlePostcode.bind(this);
    this.handleFormSubmit = this.handleFormSubmit.bind(this);
    this.handleClearForm = this.handleClearForm.bind(this);
  }

  /* This lifecycle hook gets executed when the component mounts */

  handleSuburb(e) {
    let value = e.target.value;
    console.log(value);
    this.setState(
      prevState => ({ submission: { ...prevState.submission, suburb: value } }),
      () => console.log(this.state.submission)
    );
  }

  handlePostcode(e) {
    let value = e.target.value;
    this.setState(
      prevState => ({
        submission: { ...prevState.submission, postcode: value }
      }),
      () => console.log(this.state.submission)
    );
  }

  handleState(e) {
    let value = e.target.value;
    console.log(value);
    this.setState(
      prevState => ({ submission: { ...prevState.submission, cState: value } }),
      () => console.log(this.state.submission)
    );
  }

  handleSelect(e) {
    console.log("Inside handleSelect");
    let value = e.target.value;
    this.setState(
      prevState => ({
        submission: {
          ...prevState.cState,
          cState: value
        }
      }),
      () => console.log([this.state.submission.cState])
    );
  }
  static handlePostcodeSearch(localities, PostCode) {
    if (PostCode && localities !== undefined) {
      if (localities.hasOwnProperty("locality")) {
        console.log(localities.locality);
        return localities.locality.find(
          ({ postcode }) => postcode === parseInt(PostCode)
        );
      }
    } else {
      return localities;
    }
  }
  handleValidation() {
    return this.form.current.reportValidity();
  }
  handleFormSubmit(e) {
    console.log("SUBMISSION");
    e.preventDefault();
    let userData = this.state.submission;
    let PostCode = userData.postcode;
    let queryParams = {};
    queryParams["q"] = userData.suburb;
    queryParams["state"] = userData.cState;

    let parms = Object.keys(queryParams)
      .map(key => key + "=" + queryParams[key])
      .join("&");
    if (this.handleValidation()) {
      fetch("http://localhost:8081/?" + parms, {
        method: "GET",
        headers: {
          Accept: "application/json",
          "auth-key": "872608e3-4530-4c6a-a369-052accb03ca8"
        }
      }).then(response => {
        response.json().then(data => {
          this.handleSuccessResult(
            this.constructor.handlePostcodeSearch(data.localities, PostCode)
          );
          console.log(this.state.submission.successResult);
        });
      });
    }
  }

  handleSuccessResult(result) {
    let Message = "";
    if (result) {
      Message = "The postcode, suburb and state entered are valid.";
      if (result.hasOwnProperty("locality"))
        Message = "The suburb and state entered are valid.";

      this.setState(
        prevState => ({
          submission: { ...prevState.submission, successResult: { Message } },
          show: "show",
          status: "success"
        }),
        () => console.log(this.state.submission)
      );
    } else {
      if (this.state.submission.postcode === "") {
        Message =
          "The suburb " +
          this.state.submission.suburb +
          " does not exist in the state " +
          this.state.submission.cState +
          "";
      } else {
        Message =
          "The postcode " +
          this.state.submission.postcode +
          " does not matcht the suburb " +
          this.state.submission.suburb;

        if (this.state.submission.cState !== "") {
          Message += " in the state " + this.state.submission.cState;
        }
      }
      this.setState(
        prevState => ({
          submission: {
            ...prevState.submission,
            successResult: { Message }
          },
          show: "show",
          status: "warning"
        }),
        () => console.log(this.state.submission)
      );
    }
  }

  handleClearForm(e) {
    console.log("CLEAR");
    e.preventDefault();
    this.setState(
      {
        submission: {
          suburb: "",
          postcode: "",
          cState: "",
          successResult: { Message: "" }
        },
        show: "hide"
      },
      () => console.log(this.state.submission)
    );
  }

  render() {
    return (
      <form
        className="container-fluid"
        onSubmit={this.handleFormSubmit}
        ref={this.form}
      >
        <Input
          type={"text"}
          title={"Postcode"}
          name={"postcode"}
          required={false}
          value={this.state.submission.postcode || ""}
          placeholder={"Enter Postcode"}
          onChange={this.handlePostcode}
        />{" "}
        {/* Postcode */}
        <Input
          type={"text"}
          title={"Suburb"}
          name={"suburb"}
          required={true}
          value={this.state.submission.suburb || ""}
          placeholder={"Enter Suburb"}
          onChange={this.handleSuburb}
        />{" "}
        {/* suburb */}
        <Select
          title={"State"}
          name={"cstate"}
          options={this.state.satesOptions}
          value={this.state.submission.cState || ""}
          placeholder={"Select State"}
          onChange={this.handleState}
        />{" "}
        {/* State Selection */}
        <Button
          action={this.handleFormSubmit}
          type={"primary"}
          title={"Submit"}
          style={buttonStyle}
        />{" "}
        {/*Submit */}
        <Button
          action={this.handleClearForm}
          type={"secondary"}
          title={"Clear"}
          style={buttonStyle}
        />{" "}
        {/* Clear the form */}
        <Output
          show={this.state.show}
          data={this.state.submission.successResult}
          status={this.state.status}
        />
      </form>
    );
  }
}

const buttonStyle = {
  margin: "10px 10px 10px 10px"
};

export default FormContainer;
