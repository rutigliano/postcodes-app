const fetch = require('node-fetch');
const express = require('express');
const cors = require('cors');
const app = express();
const port = 8081;


app.use(cors());
app.get('/', (req, res) => {
    const q = req.query.q;
    const state = req.query.state;

    fetch('https://digitalapi.auspost.com.au/postcode/search.json?q='+q+'&state='+state,{
        method: "GET",
        headers: {
          Accept: "application/json",
          "auth-key": "872608e3-4530-4c6a-a369-052accb03ca8"
        }
      }).then(response => {
        response.json().then(data => {
            res.send(data);
        });
      });

});

app.listen(port, () => console.log(` App running on http://localhost:3000/ , CORS Middleware Listening on port ${port}!`))